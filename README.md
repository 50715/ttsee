Инструменты тестирования: Эмуляция торгового оборудования
=========================

Описание
-------------------------

Расширение которое подменяет системные вызовы библиотеки *БПО* для эмуляции наличия торгового оборудования.

Доступные к эмуляции системы
-------------------------

1. Электронные весы
2. Сканер штрихкода
3. Принтер этикеток

Особенности
-------------------------

1. Электронные весы

    Возвращают случайный вес из указанного интервала

2. Сканер штрихкода

    Может запросить штрихкод или вернуть штрихкод из списка

3. Принтер этикеток

    Может сгенерировать этикетку и вернуть её как картинку

4. Возможность выбора оборудования работа которого будет эмулироваться

5. Для работы в безопасном режиме надо создать профиль разрешающий расширение всех модулей, а также обращение к внешним ресурсам

Системные требования
-------------------------

- Платформа 1С не ниже 8.3.14
- БПО версии 2.1.4.2
