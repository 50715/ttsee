﻿
&Вместо("ПодключитьУстройство")
Функция ttsee_ПодключитьУстройство(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры)
	
	ИдентификаторУстройства = Неопределено;
	Если ТипЗнч(Параметры) = Тип("Структура")
		И Параметры.Свойство("Идентификатор", ИдентификаторУстройства)
		И ТипЗнч(ИдентификаторУстройства) = Тип("СправочникСсылка.ПодключаемоеОборудование")
		Тогда 
		Эмуляция = гл_ttsee_СимуляцияОборудования[ИдентификаторУстройства];
		Если Эмуляция <> Неопределено Тогда
			ВыходныеПараметры = Новый Массив();
			ПараметрыПодключения.Вставить("ИДУстройства", "");
			ПараметрыПодключения.Вставить("РевизияИнтерфейса", МенеджерОборудованияКлиентПовтИсп.РевизияИнтерфейсаДрайверов());
			
			Если Эмуляция.ТипОборудования = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.СканерШтрихкода") Тогда 
				ПараметрыОткрытия = Новый Структура;
				ПараметрыОткрытия.Вставить("ИдентификаторУстройства", ИдентификаторУстройства);
				ОткрытьФорму("ОбщаяФорма.ttsee_ФормаВводаШтрихкода", ПараметрыОткрытия);
			ИначеЕсли Эмуляция.ТипОборудования = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.ЭлектронныеВесы") Тогда 
				гл_ttsee_ТарированныеВесы.Вставить(ИдентификаторУстройства, 0);
			КонецЕсли;
			
			Возврат Истина;
		КонецЕсли;
	КонецЕсли;
	
	Результат = ПродолжитьВызов(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры);
	Возврат Результат;
КонецФункции

&Вместо("ОтключитьУстройство")
Функция ttsee_ОтключитьУстройство(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры)
	
	ИдентификаторУстройства = Неопределено;
	Если ТипЗнч(Параметры) = Тип("Структура")
		И Параметры.Свойство("Идентификатор", ИдентификаторУстройства)
		И ТипЗнч(ИдентификаторУстройства) = Тип("СправочникСсылка.ПодключаемоеОборудование")
		Тогда 
		Эмуляция = гл_ttsee_СимуляцияОборудования[ИдентификаторУстройства];
		Если Эмуляция <> Неопределено Тогда
			ВыходныеПараметры = Новый Массив();
			
			Если Эмуляция.ТипОборудования = ПредопределенноеЗначение("Перечисление.ТипыПодключаемогоОборудования.СканерШтрихкода") Тогда 
				Оповестить("ОтключениеСимулируемогоОборудования", ИдентификаторУстройства, "ЭмуляцияТорговогоОборудования");
			КонецЕсли;
			
			Возврат Истина;
		КонецЕсли;
	КонецЕсли;
	
	Результат = ПродолжитьВызов(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры);
	Возврат Результат;
КонецФункции

&Вместо("ТестУстройства")
Функция ttsee_ТестУстройства(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры)
	
	ИдентификаторУстройства = Неопределено;
	Если ТипЗнч(Параметры) = Тип("Структура")
		И Параметры.Свойство("Идентификатор", ИдентификаторУстройства)
		И ТипЗнч(ИдентификаторУстройства) = Тип("СправочникСсылка.ПодключаемоеОборудование")
		Тогда 
		Эмуляция = гл_ttsee_СимуляцияОборудования[ИдентификаторУстройства];
		Если Эмуляция <> Неопределено Тогда
			ВыходныеПараметры.Очистить();
			ВыходныеПараметры.Добавить(0);
			ВыходныеПараметры.Добавить("true");	//	РезультатТеста
			ВыходныеПараметры.Добавить("true");	//	АктивированДемоРежим
			Возврат Истина;
		КонецЕсли;
	КонецЕсли;
	
	Результат = ПродолжитьВызов(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры);
	Возврат Результат;
КонецФункции

&Вместо("ПолучитьВес")
Функция ttsee_ПолучитьВес(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры)
	
	ИдентификаторУстройства = Неопределено;
	Если ТипЗнч(Параметры) = Тип("Структура")
		И Параметры.Свойство("Идентификатор", ИдентификаторУстройства)
		И ТипЗнч(ИдентификаторУстройства) = Тип("СправочникСсылка.ПодключаемоеОборудование")
		Тогда 
		Эмуляция = гл_ttsee_СимуляцияОборудования[ИдентификаторУстройства];
		Если Эмуляция <> Неопределено Тогда
			Генератор = Новый ГенераторСлучайныхЧисел(ТекущаяУниверсальнаяДатаВМиллисекундах());
			Вес = Эмуляция.НачалоДиапазона 
				+ (Эмуляция.ОкончаниеДиапазона - Эмуляция.НачалоДиапазона)
				* Генератор.СлучайноеЧисло(1, 99999) / 99999
				- гл_ttsee_ТарированныеВесы[ИдентификаторУстройства];
			ВыходныеПараметры.Очистить();  
			ВыходныеПараметры.Добавить(Вес);
			Возврат Истина;
		КонецЕсли;
	КонецЕсли;
	
	Результат = ПродолжитьВызов(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры);
	Возврат Результат;
КонецФункции

&Вместо("Тарировать")
Функция ttsee_Тарировать(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры, ВесТары)
	
	ИдентификаторУстройства = Неопределено;
	Если ТипЗнч(Параметры) = Тип("Структура")
		И Параметры.Свойство("Идентификатор", ИдентификаторУстройства)
		И ТипЗнч(ИдентификаторУстройства) = Тип("СправочникСсылка.ПодключаемоеОборудование")
		Тогда 
		Эмуляция = гл_ttsee_СимуляцияОборудования[ИдентификаторУстройства];
		Если Эмуляция <> Неопределено Тогда
			КвалификаторыЧисла = Новый КвалификаторыЧисла(10, 3, ДопустимыйЗнак.Неотрицательный);
			ОписаниеТипа = Новый ОписаниеТипов("Число",,,КвалификаторыЧисла);
			Вес = ОписаниеТипа.ПривестиЗначение(ВесТары);
			гл_ttsee_ТарированныеВесы.Вставить(ИдентификаторУстройства, Вес);
			ВыходныеПараметры.Очистить();  
			Возврат Истина;
		КонецЕсли;
	КонецЕсли;
	
	Результат = ПродолжитьВызов(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры, ВесТары);
	Возврат Результат;
КонецФункции

&Вместо("ИнициализацияПринтера")
Функция ttsee_ИнициализацияПринтера(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры)
	
	ИдентификаторУстройства = Неопределено;
	Если ТипЗнч(Параметры) = Тип("Структура")
		И Параметры.Свойство("Идентификатор", ИдентификаторУстройства)
		И ТипЗнч(ИдентификаторУстройства) = Тип("СправочникСсылка.ПодключаемоеОборудование")
		Тогда 
		Эмуляция = гл_ttsee_СимуляцияОборудования[ИдентификаторУстройства];
		Если Эмуляция <> Неопределено Тогда
			ВыходныеПараметры.Очистить();  
			Возврат Истина;
		КонецЕсли;
	КонецЕсли;
	
	Результат = ПродолжитьВызов(ОбъектДрайвера, Параметры, ПараметрыПодключения, ВыходныеПараметры);
	Возврат Результат;
КонецФункции

&Вместо("ПечатьЭтикеток")
Функция ttsee_ПечатьЭтикеток(ОбъектДрайвера, Параметры, ПараметрыПодключения, ШаблонЭтикетки, МассивЭтикеток, ВыходныеПараметры)
	
	ИдентификаторУстройства = Неопределено;
	Если ТипЗнч(Параметры) = Тип("Структура")
		И Параметры.Свойство("Идентификатор", ИдентификаторУстройства)
		И ТипЗнч(ИдентификаторУстройства) = Тип("СправочникСсылка.ПодключаемоеОборудование")
		Тогда 
		Эмуляция = гл_ttsee_СимуляцияОборудования[ИдентификаторУстройства];
		Если Эмуляция <> Неопределено Тогда
			ВыходныеПараметры.Очистить();  
			Возврат Истина;
		КонецЕсли;
	КонецЕсли;
	
	Результат = ПродолжитьВызов(ОбъектДрайвера, Параметры, ПараметрыПодключения, ШаблонЭтикетки, МассивЭтикеток, ВыходныеПараметры);
	Возврат Результат;
КонецФункции
